module.exports = function(grunt) {
    'use strict';

    var path = require('path'),
        _ = require('underscore'),
        exec = require('child_process').exec,
        async = require('async');

    var _defaults = {
        nodeModulesBinDir : 'node_modules/.bin',
        app : 'cordova'
    };

    // utils
    function _createCacheDir(dirPath){
        if(!grunt.file.exists(dirPath)){
            grunt.file.mkdir(dirPath);
        }
    }

    function _resolvedPromise(){
        return new Promise(function(resolve, reject){
            resolve();
        });
    }

    function _execCommand(cmd, cwd){
        return new Promise(function(resolve, reject){
            cwd = cwd || '.';
            exec(cmd, {'cwd' : cwd},function(err, stdout, stderr){
                if(err){
                    return reject(err);
                }
                resolve();
            });
        });
    }

    function _cloneGit(gitUrl, cachedPath, gitTag){
        return new Promise(function(resolve, reject){
            var cmd = [
                'git clone --depth=1',
                gitTag ? '-b ' + gitTag : '',
                gitUrl,
                cachedPath
            ].join(' ');
            _execCommand(cmd)
                .then(function(){
                    grunt.log.writeln('git clonned: ' + gitUrl + ' -> ' + cachedPath);
                    resolve();
                }).catch(function(err){
                    reject(err);
                });
        });
    }

    function _cloneDir(dirPath, cachedPath){
        return new Promise(function(resolve, reject){
            var cmd = [
                'cp -r',
                dirPath,
                cachedPath
            ].join(' ');
            _execCommand(cmd)
                .then(function(){
                    grunt.log.writeln('dir clonned: ' + dirPath + ' -> ' + cachedPath);
                    resolve();
                }).catch(function(err){
                    reject(err);
                });
        });
    }

    function _getCloneFunction(cachedPath, gitUrl, tag, dirPath, objName){
        var cloneFunction;
        if(!grunt.file.exists(cachedPath)){
            if(_.isUndefined(gitUrl)){
                // local folder
                cloneFunction = _.bind(_cloneDir, grunt, dirPath, cachedPath);
            }else{
                cloneFunction = _.bind(_cloneGit, grunt, gitUrl, cachedPath, tag);
            }
        }else{
            cloneFunction = function(){
                grunt.log.writeln('installing from cache: ' + objName);
                return _resolvedPromise(true);
            };
        }
        return cloneFunction;
    }

    /*
    *   Creates cordova app with local caching of app template
    *
    *   Configuration options:
    *   {
    *       nodeModulesBinDir : 'path to node_modules/.bin', // default 'node_modules/.bin'
    *       appName : 'AppName',
    *       appNamespace : 'app.namespace',
    *       appDir : 'path/2/phonegap/app/dir', // default '.'
    *       gitUrl : 'url for cloning git repository',
    *       dirPath : 'path to local dir with app template'
    *       templateId : 'template id for cache'
    *       cacheDir : 'path/2/[honegap/plugins/cache/dir', // default '.templates-cache'
    *       tag : "optional tag for fetching specific version"
    *   }
    *
    *   gitUrl or dirPath must be specified
    */
    grunt.registerMultiTask(
        'cordova-app-create',
        'creates cordova app with local caching of app template',
        function(){

            var done = this.async(),
                config = grunt.config.get("cordova-app-create"),
                cfg = config[this.target];

            _.defaults(cfg, _defaults, { cacheDir : '.templates-cache' });

            _createCacheDir(cfg.cacheDir);
            var cachedPath = path.join(cfg.cacheDir, cfg.templateId);

            var installFromCache = function(){
                var cmd = [
                    path.join(cfg.nodeModulesBinDir, cfg.app),
                    'create',
                    cfg.appDir,
                    cfg.appNamespace,
                    cfg.appName,
                    '--template',
                    cachedPath
                ].join(' ');
                return _execCommand(cmd);
            };

            var cloneFunction = _getCloneFunction(cachedPath, cfg.gitUrl, cfg.tag, cfg.dirPath, cfg.templateId);

            cloneFunction()
                .then(installFromCache)
                .then(function(){
                    grunt.log.writeln('app created from cache: ' + cfg.templateId);
                    done();
                })
                .catch(function(err){
                    grunt.fail.warn(err);
                    done();
                });
        }
    );

    /*
    *   Adds Cordova platform with caching in local dir
    *
    *   Configuration options:
    *   {
    *       nodeModulesBinDir : 'path to node_modules/.bin', // default 'node_modules/.bin'
    *       platform : 'platform name',
    *       gitUrl : 'url for cloning git repository',
    *       dirPath : 'path to local dir with app template'
    *       cacheDir : 'path/2/[honegap/plugins/cache/dir', // default '.platforms-cache'
    *       tag : "optional tag for fetching specific version"
    *   }
    *
    *   gitUrl or dirPath must be specified
    */
    grunt.registerMultiTask(
        'cordova-platform-add',
        'add cordova platform with local caching',
        function(){
            
            var done = this.async(),
                config = grunt.config.get('cordova-platform-add'),
                cfg = config[this.target];

            _.defaults(cfg, _defaults, { cacheDir : '.platforms-cache' });

            _createCacheDir(cfg.cacheDir);

            var cachedPath = path.join(cfg.cacheDir, cfg.platform);

            var installFromCache = function(){
                var cmd = [
                    path.join(cfg.nodeModulesBinDir, cfg.app),
                    'platform add',
                    cachedPath
                ].join(' ');
                return _execCommand(cmd);
            };

            var cloneFunction = _getCloneFunction(cachedPath, cfg.gitUrl, cfg.tag, cfg.dirPath, cfg.platform);

            cloneFunction()
                .then(installFromCache)
                .then(function(){
                    grunt.log.writeln('platform added from cache: ' + cfg.platform);
                    done();
                })
                .catch(function(err){
                    grunt.fail.warn(err);
                    done();
                });
        }
    );

    /*
    *   Installs cordova plugins by list with caching in local dir
    *
    *   Configuration options:
    *   {
            nodeModulesBinDir : 'path to node_modules/.bin', // default 'node_modules/.bin'
    *       appDir : 'path/2/cordova/app/dir', // default '.'
    *       cacheDir : 'path/2/cordova/plugins/cache/dir', // default '.plugins-cache'
    *       plugins : [
    *           {
    *               id : "id of the plugin",
    *               gitUrl : "url for cloning git repository",
    *               tag : "optional tag for fetching specific version"
    *           },
    *           {
    *               id : "id of the plugin",
    *               dirPath : "path to the folder with plugin",
    *               tag : "optional tag for fetching specific version"
    *           },
    *           . . .
    *       ]
    *   }
    */
    grunt.registerMultiTask(
        'cordova-plugins-install',
        'install required cordova plugins with local caching of plugins',
        function(){
            
            var done = this.async(),
                config = grunt.config.get('cordova-plugins-install'),
                cfg = config[this.target];

            _.defaults(cfg, _defaults, { cacheDir : '.plugins-cache' });

            _createCacheDir(cfg.cacheDir);

            async.eachSeries(cfg.plugins,
                function iterator(plugin, cb){
                    var cachedPath = path.join(cfg.cacheDir, plugin.id);

                    var installFromCache = function(){
                        var cmd = [
                            path.join(cfg.nodeModulesBinDir, cfg.app),
                            'plugin add',
                            cachedPath
                        ].join(' ');
                        return _execCommand(cmd);
                    };

                    var cloneFunction = _getCloneFunction(cachedPath, plugin.gitUrl, plugin.tag, plugin.dirPath, plugin.id);
                    cloneFunction()
                        .then(installFromCache)
                        .then(function(){
                            grunt.log.writeln('plugin installed from cache: ' + plugin.id);
                            cb();
                        })
                        .catch(function(err){
                            grunt.fail.warn(err);
                            cb(err);
                        });

                },
                function finished(err){
                    if(err){
                        return grunt.fail.warn(err);
                    }
                    grunt.log.ok('plugins installed');
                    done();
                }
            );
        }
    );
};